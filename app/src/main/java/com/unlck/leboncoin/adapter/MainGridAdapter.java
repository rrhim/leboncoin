package com.unlck.leboncoin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.unlck.leboncoin.R;
import com.unlck.leboncoin.listener.OnRecyclerViewItemClickListener;
import com.unlck.leboncoin.viewholder.MainGridViewHolder;

import java.util.ArrayList;
import java.util.List;

public class MainGridAdapter extends RecyclerView.Adapter<MainGridViewHolder> {

    private Context mContext;
    private List<Integer> mAlbumList;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;


    // Provide a suitable constructor (depends on the kind of dataset)
    public MainGridAdapter(Context context, List<Integer> albumList) {
        mContext = context;
        mAlbumList = albumList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MainGridViewHolder onCreateViewHolder(ViewGroup parent,
                                                 int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_list_main, parent, false);

        return new MainGridViewHolder(view, onRecyclerViewItemClickListener);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MainGridViewHolder holder, int position) {
        holder.mTitleTextView.setText(mContext.getResources().getString(R.string.section_title, mAlbumList.get(position)));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mAlbumList.size();
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    public void updateList(ArrayList<Integer> updatedList) {
        mAlbumList = updatedList;
        this.notifyDataSetChanged();
    }
}
