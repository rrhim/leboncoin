package com.unlck.leboncoin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.unlck.leboncoin.R;
import com.unlck.leboncoin.listener.OnRecyclerViewItemClickListener;
import com.unlck.leboncoin.model.Photo;
import com.unlck.leboncoin.viewholder.PhotoGridViewHolder;

import java.util.ArrayList;
import java.util.List;

public class PhotoGridAdapter extends RecyclerView.Adapter<PhotoGridViewHolder> {

    private List<Photo> mPhotoList;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public PhotoGridAdapter(List<Photo> photoList) {
        mPhotoList = photoList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PhotoGridViewHolder onCreateViewHolder(ViewGroup parent,
                                                  int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_list_photo, parent, false);

        return new PhotoGridViewHolder(view, onRecyclerViewItemClickListener);

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PhotoGridViewHolder holder, int position) {

        Photo photo = mPhotoList.get(position);

        Picasso.get()
                .load(photo.getThumbnailUrl())
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image_broken)
                .fit()
                .into(holder.mPhotoImageView);

        holder.mTitleTextView.setText(photo.getTitle());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mPhotoList.size();
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    public void updateList(ArrayList<Photo> updatedList) {
        mPhotoList = updatedList;
        this.notifyDataSetChanged();
    }
}
