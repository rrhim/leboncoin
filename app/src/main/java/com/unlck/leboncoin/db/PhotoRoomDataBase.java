package com.unlck.leboncoin.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.unlck.leboncoin.db.dao.PhotoDao;
import com.unlck.leboncoin.model.Photo;

@Database(entities = {Photo.class},
        version = 1, exportSchema = false)

public abstract class PhotoRoomDataBase extends RoomDatabase {

    private static PhotoRoomDataBase instance;

    protected PhotoRoomDataBase() {
    }

    public static PhotoRoomDataBase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    PhotoRoomDataBase.class, "photosboncoin.db")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract PhotoDao photoDao();
}