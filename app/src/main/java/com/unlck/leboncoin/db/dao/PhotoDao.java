package com.unlck.leboncoin.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.unlck.leboncoin.model.Photo;

import java.util.List;

@Dao
public interface PhotoDao {

    @Query("SELECT * FROM photos")
    LiveData<List<Photo>> getPhotos();

    @Query("SELECT * FROM photos WHERE id=:id")
    Photo getPhoto(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPhotos(List<Photo> photos);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPhoto(Photo photo);

    @Delete
    void deleteMission(Photo photo);

    @Update
    void updateMission(Photo photo);
}
