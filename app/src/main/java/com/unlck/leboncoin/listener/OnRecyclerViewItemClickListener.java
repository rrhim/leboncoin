package com.unlck.leboncoin.listener;

import android.view.View;

public interface OnRecyclerViewItemClickListener {
    void onItemClick(int position, View view);
}
