package com.unlck.leboncoin.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.unlck.leboncoin.db.PhotoRoomDataBase;
import com.unlck.leboncoin.model.Photo;
import com.unlck.leboncoin.service.PhotoRetrofitService;
import com.unlck.leboncoin.service.PhotoService;

import java.util.List;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoRepository {


    private static PhotoRepository instance;
    private PhotoRoomDataBase photoRoomDataBase;
    private PhotoRetrofitService photoRetrofitService;
    private Executor executor;

    private PhotoRepository(Context context, Executor executor) {
        photoRoomDataBase = PhotoRoomDataBase.getInstance(context);
        photoRetrofitService = PhotoService.getInstance();
        this.executor = executor;
    }

    public static PhotoRepository getInstance(Context context, Executor executor) {
        if (instance == null) {
            instance = new PhotoRepository(context, executor);
        }
        return instance;
    }

    public LiveData<List<Photo>> getPhotos() {
        refreshPhoto(); // try to refresh data if possible
        return photoRoomDataBase.photoDao().getPhotos(); // return a LiveData directly from the database.
    }

    private void refreshPhoto() {
        executor.execute(() -> {
            PhotoService.getInstance()
                    .getPhotos()
                    .enqueue(new Callback<List<Photo>>() {
                        @Override
                        public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                            executor.execute(() -> {
                                photoRoomDataBase.photoDao().insertPhotos(response.body());
                            });
                        }

                        @Override
                        public void onFailure(Call<List<Photo>> call, Throwable t) {
                            Log.d("PhotoRepository", "Problem getting data");
                        }
                    });
        });
    }
}