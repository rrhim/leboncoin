package com.unlck.leboncoin.service;

import com.unlck.leboncoin.model.Photo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PhotoRetrofitService {
    @GET(PhotoService.PATH_PHOTOS)
    Call<List<Photo>> getPhotos();
}
