package com.unlck.leboncoin.service;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PhotoService {
    public static final String PHOTOS_URL = "https://jsonplaceholder.typicode.com/"; // WS Not working With http://
    public static final String PATH_PHOTOS = "photos";

    private static PhotoRetrofitService instance;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    public static PhotoRetrofitService getInstance() {
        if (instance == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(PHOTOS_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            instance = retrofit.create(PhotoRetrofitService.class);
        }
        return instance;
    }
}
