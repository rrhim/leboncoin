package com.unlck.leboncoin.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.unlck.leboncoin.R;
import com.unlck.leboncoin.adapter.MainGridAdapter;
import com.unlck.leboncoin.adapter.PhotoGridAdapter;
import com.unlck.leboncoin.customview.ItemOffsetDecoration;
import com.unlck.leboncoin.listener.OnRecyclerViewItemClickListener;
import com.unlck.leboncoin.model.Photo;

import java.util.ArrayList;
import java.util.List;

import static androidx.recyclerview.widget.RecyclerView.VERTICAL;

public class AlbumActivity extends AppCompatActivity implements OnRecyclerViewItemClickListener,PhotoDialogFragment.OnFragmentInteractionListener {

public static final String PHOTO_SHOW_DIALOG_FRAGMENT_TAG="photo_show_dialog_fragment_tag";

    private RecyclerView mRecyclerView;

    private PhotoGridAdapter mAdapter;
    private GridLayoutManager mManager;

    private List<Photo> mPhotoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        mRecyclerView=findViewById(R.id.recyclerview_photo);

        setData();
        setView();
    }

    private void setData(){
        mPhotoList= new ArrayList<>();
        mPhotoList= getIntent().getParcelableArrayListExtra(MainActivity.ALBUM_KEY);
    }

    private void setView(){
        setRecyclerView();
    }

    private void setRecyclerView(){
        mRecyclerView.setHasFixedSize(true);
        int numberOfColumns = 3;
        mManager = new GridLayoutManager(this,
                numberOfColumns,
                VERTICAL,
                false);
        mRecyclerView.setLayoutManager(mManager);

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this,
                R.dimen.item_offset_small);
        mRecyclerView.addItemDecoration(itemDecoration);
        mAdapter = new PhotoGridAdapter(mPhotoList);

        mAdapter.setOnRecyclerViewItemClickListener(AlbumActivity.this);

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(int position, View view) {
        FragmentManager fm = getSupportFragmentManager();
        PhotoDialogFragment photoDialogFragment = PhotoDialogFragment.newInstance(
                mPhotoList.get(position));
        photoDialogFragment.show(fm,PHOTO_SHOW_DIALOG_FRAGMENT_TAG);
    }

    @Override
    public void onFragmentInteraction() {
        Fragment dialogAddEdit = getSupportFragmentManager().findFragmentByTag(
                PHOTO_SHOW_DIALOG_FRAGMENT_TAG);
        if (dialogAddEdit != null) {
            DialogFragment df = (DialogFragment) dialogAddEdit;
            df.dismiss();
        }
    }
}
