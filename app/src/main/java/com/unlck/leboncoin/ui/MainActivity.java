package com.unlck.leboncoin.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.unlck.leboncoin.R;
import com.unlck.leboncoin.adapter.MainGridAdapter;
import com.unlck.leboncoin.customview.ItemOffsetDecoration;
import com.unlck.leboncoin.listener.OnRecyclerViewItemClickListener;
import com.unlck.leboncoin.model.Photo;
import com.unlck.leboncoin.repository.PhotoRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;

import static androidx.recyclerview.widget.RecyclerView.VERTICAL;

public class MainActivity extends AppCompatActivity implements OnRecyclerViewItemClickListener {

    public static final String ALBUM_KEY = "album";

    private RecyclerView mRecyclerView;

    private MainGridAdapter mAdapter;

    private List<Integer> mAlbumList;
    private Map<Integer, List<Photo>> mPhotoMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recyclerview_album);

        setView();
        fillPhotosList();
    }

    private void setView() {
        setRecyclerView();
    }

    private void fillPhotosList() {
        ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.dialog_loading_label));
        pd.show();

        PhotoRepository.getInstance(this, Executors.newSingleThreadExecutor()).getPhotos().observe(
                this,
                new Observer<List<Photo>>() {
                    @Override
                    public void onChanged(List<Photo> photos) {

                        mPhotoMap = getByAlbum(photos);
                        Set albumList = mPhotoMap.keySet();

                        mAlbumList.clear();
                        mAlbumList.addAll(albumList);

                        mAdapter.updateList((ArrayList<Integer>) mAlbumList);

                        if (pd.isShowing()) {
                            pd.dismiss();
                        }
                    }
                });
    }

    private void setRecyclerView() {
        mAlbumList = new ArrayList<>();
        mRecyclerView.setHasFixedSize(true);
        int numberOfColumns = 3;
        GridLayoutManager mManager = new GridLayoutManager(this,
                numberOfColumns,
                VERTICAL,
                false);
        mRecyclerView.setLayoutManager(mManager);

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this,
                R.dimen.item_offset_small);
        mRecyclerView.addItemDecoration(itemDecoration);
        mAdapter = new MainGridAdapter(this, mAlbumList);

        mAdapter.setOnRecyclerViewItemClickListener(MainActivity.this);

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(int position, View view) {
        Intent intent = new Intent(MainActivity.this, AlbumActivity.class);
        intent.putParcelableArrayListExtra(ALBUM_KEY,
                (ArrayList<? extends Parcelable>) mPhotoMap.get(mAlbumList.get(position)));
        startActivity(intent);
    }

    private Map<Integer, List<Photo>> getByAlbum(List<Photo> photoList) {
        Map<Integer, List<Photo>> map = new HashMap<>();

        for (Photo photo : photoList) {
            int key = photo.getAlbumId();
            if (map.containsKey(key)) {
                List<Photo> list = map.get(key);
                list.add(photo);
            } else {
                List<Photo> list = new ArrayList<Photo>();
                list.add(photo);
                map.put(key, list);
            }
        }

        return map;
    }
}
