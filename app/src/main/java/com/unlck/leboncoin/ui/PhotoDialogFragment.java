package com.unlck.leboncoin.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.squareup.picasso.Picasso;
import com.unlck.leboncoin.R;
import com.unlck.leboncoin.model.Photo;

/**
 * A simple {@link DialogFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PhotoDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PhotoDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PhotoDialogFragment extends DialogFragment {
    private static final String ARG_PHOTO = "photo";

    private AppCompatImageView mImage;
    private AppCompatTextView mTitle;
    private AppCompatButton mButton;

    private Photo mPhoto;

    private OnFragmentInteractionListener mListener;

    public PhotoDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param photo photo.
     * @return A new instance of fragment PhotoDialogFragment.
     */

    public static PhotoDialogFragment newInstance(Photo photo) {
        PhotoDialogFragment fragment = new PhotoDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PHOTO, photo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPhoto = getArguments().getParcelable(ARG_PHOTO);
        }
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MyDialogFragmentStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photo_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mImage = view.findViewById(R.id.image_photo);
        mTitle = view.findViewById(R.id.text_photo);
        mButton = view.findViewById(R.id.dialog_photo_close_button);

        setCancelable(false);

        setView();
        setListener();
    }

    private void setView() {
        mTitle.setText(mPhoto.getTitle());
        Picasso.get()
                .load(mPhoto.getUrl())
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image_broken)
                .fit()
                .into(mImage);
    }

    private void setListener() {
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCloseButtonPressed();
            }
        });
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    public void onCloseButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }
}
