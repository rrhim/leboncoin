package com.unlck.leboncoin.util;

public class ConstantPhotoUtil {
    public static final String PHOTO_ALBUM_ID="albumId";
    public static final String PHOTO_ID="id";
    public static final String PHOTO_TITLE="title";
    public static final String PHOTO_URL="url";
    public static final String PHOTO_THUMBNAIL_URL="thumbnailUrl";
}

