package com.unlck.leboncoin.viewholder;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.unlck.leboncoin.R;
import com.unlck.leboncoin.listener.OnRecyclerViewItemClickListener;

public class MainGridViewHolder extends RecyclerView.ViewHolder {

    public AppCompatTextView mTitleTextView;

    public MainGridViewHolder(View itemView,
                              final OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {

        super(itemView);
        mTitleTextView = itemView.findViewById(R.id.text_grid_item);

        mTitleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onRecyclerViewItemClickListener != null) {
                    onRecyclerViewItemClickListener.onItemClick(getAdapterPosition(), view);
                }
            }
        });
    }
}
