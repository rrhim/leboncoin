package com.unlck.leboncoin.viewholder;

import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.unlck.leboncoin.R;
import com.unlck.leboncoin.listener.OnRecyclerViewItemClickListener;

public class PhotoGridViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout mLayoutContainer;
    public AppCompatImageView mPhotoImageView;
    public AppCompatTextView mTitleTextView;

    public PhotoGridViewHolder(View itemView,
                               final OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {

        super(itemView);
        mLayoutContainer = itemView.findViewById(R.id.layout_container);
        mPhotoImageView = itemView.findViewById(R.id.image_grid_item);
        mTitleTextView = itemView.findViewById(R.id.text_grid_item);

        mLayoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onRecyclerViewItemClickListener != null) {
                    onRecyclerViewItemClickListener.onItemClick(getAdapterPosition(), view);
                }
            }
        });
    }
}
